namespace RiseFood.ProductList.Domain
{
    public enum Size
    {
        STANDARD,
        SMALL,
        MEDIUM,
        LARGE
    }
}